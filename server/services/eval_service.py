import logging

LOG = logging.getLogger(__name__)


def safe_eval(expr_str):
    LOG.debug('calculate: ' + str(expr_str))
    try:
        return eval(expr_str, {"__builtins__": None}, None)
    except Exception as ex:
        LOG .warning('Exception evaluating expression [' + str(expr_str) +
                     '] except: ' + str(ex))
        return None
