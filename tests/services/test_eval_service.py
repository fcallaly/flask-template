from server.services import eval_service


def test_simple_eval():
    assert eval_service.safe_eval('21 * 4') == 84


def test_not_allowed_import():
    assert eval_service.safe_eval('import os') is None


def test_not_allowed_open():
    assert eval_service.safe_eval(
                    'open("test.py") as f: f.readlines()') is None


def test_not_allowed_vars():
    assert eval_service.safe_eval('my_var=10') is None
